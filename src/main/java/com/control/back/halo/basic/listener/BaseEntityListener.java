package com.control.back.halo.basic.listener;

import java.time.LocalDateTime;

import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;

import org.springframework.stereotype.Component;

import com.control.back.halo.basic.entity.BaseEntity;
import com.control.back.halo.basic.utils.UserUtils;

@Component("baseEntityListener")
public class BaseEntityListener {

    /**
     * Before add entity, init createDate and updateDate
     * 
     * @param baseEntity
     */
    @PrePersist
    public void initEntity(BaseEntity baseEntity) {
        baseEntity.setCreatedDate(LocalDateTime.now());
        baseEntity.setCreatedBy(UserUtils.getCurrentUser());
        baseEntity.setLastModifiedDate(LocalDateTime.now());
        baseEntity.setLastModifiedBy(UserUtils.getCurrentUser());
    }

    /**
     * Before update entity ,set updateDate
     */
    @PreUpdate
    public void updateEntity(BaseEntity baseEntity) {
        baseEntity.setLastModifiedDate(LocalDateTime.now());
        baseEntity.setLastModifiedBy(UserUtils.getCurrentUser());
    }
}

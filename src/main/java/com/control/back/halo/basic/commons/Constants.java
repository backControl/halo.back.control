
package com.control.back.halo.basic.commons;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 常量类
 * 
 * @author
 */
public class Constants {

    /**
     * 时间戳
     */
    public final static String RESET_TIME = new SimpleDateFormat("yyyyMMddhh").format(new Date());

    /**
     * session key
     */
    public final static String USER_SESSION_ID = "user_session_id";

}
